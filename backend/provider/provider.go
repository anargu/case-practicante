package provider

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"./util"
)

// Product CORE DATA
type Product struct {
	IDItem int32  `json:"ID_ITEM"`
	DeItem string `json:"DE_ITEM"`
	DeCate string `json:"DE_CATE"`
	DeEqui string `json:"DE_EQUI"`
	DeFami string `json:"DE_FAMI"`
}

// Category -> BRANDS
type Category struct {
	Name   string    `json:"name"`
	Brands *[]string `json:"brands"`
}

// Brand -> FAMILIES
type Brand struct {
	Category string    `json:"category"`
	Name     string    `json:"name"`
	Families *[]string `json:"families"`
}

// Family -> PRODUCTS
type Family struct {
	Category   string   `json:"category"`
	Brand      string   `json:"brand"`
	Name       string   `json:"name"`
	ProductIDs *[]int32 `json:"id_products"`
}

// ProductMap [ITEM_ID] Product
type ProductMap map[int32]Product

// CategoryMap [STRING] Category
type CategoryMap map[string]Category

// BrandMap map[string]Brand
type BrandMap map[string]Brand

// FamilyMap map[string]Family
type FamilyMap map[string]Family

// QueryResponse for search
type QueryResponse struct {
	Data          []Product `json:"data"`
	RelatedSearch []Product `json:"related_search"`
	Next int `json:"next"`
}

var (
	// Products variable
	Products []Product
	// ProductsMap [ID]Product map
	ProductsMap = make(ProductMap)
	// Categories [string]Category map
	Categories = make(CategoryMap)
	// Brands [string]Brand map
	Brands = make(BrandMap)
	// Families [string]Familiy map
	Families = make(FamilyMap)
)

// InitData loads in memory all products
func InitData() {
	rawData, err := ioutil.ReadFile("./products.json")
	if err != nil {
		log.Fatal("Error at reading raw data")
		log.Fatal(err)
	}

	err = json.Unmarshal(rawData, &Products)
	if err != nil {
		log.Fatal("Error at parsing JSON")
		log.Fatal(err)
	}

	for _, product := range Products {
		ProductsMap[product.IDItem] = product
	}

	InitAllFamilies(&Families)
	InitAllBrands(&Brands, &Families)
	InitAllCategories(&Categories, &Brands)

}

// InitAllFamilies initilize FamilyMap
func InitAllFamilies(Families *FamilyMap) {

	for _, product := range Products {

		var productIds []int32
		if (*Families)[product.DeFami].ProductIDs == nil {
			productIds = []int32{product.IDItem}
		} else {
			productIds = append(*(*Families)[product.DeFami].ProductIDs, product.IDItem)
		}

		(*Families)[product.DeFami] = Family{Name: product.DeFami, Brand: product.DeEqui, Category: product.DeCate, ProductIDs: &productIds}
	}
}

// InitAllBrands initilize BrandMap
func InitAllBrands(Brands *BrandMap, Families *FamilyMap) {

	for _, product := range Products {

		var families []string

		if (*Brands)[product.DeEqui].Families == nil {
			families = []string{product.DeFami}
		} else {
			if util.IndexOfString((*Brands)[product.DeEqui].Families, product.DeFami) == -1 {
				families = append(*(*Brands)[product.DeEqui].Families, product.DeFami)
			} else {
				families = *(*Brands)[product.DeEqui].Families
			}
		}
		(*Brands)[product.DeEqui] = Brand{Category: product.DeCate, Name: product.DeEqui, Families: &families}
	}

}

// InitAllCategories initilize CategoryMap
func InitAllCategories(Categories *CategoryMap, Brands *BrandMap) {

	for _, product := range Products {

		var brands []string

		if (*Categories)[product.DeCate].Brands == nil {
			brands = []string{product.DeEqui}
		} else {
			if util.IndexOfString((*Categories)[product.DeCate].Brands, product.DeEqui) == -1 {
				brands = append(*(*Categories)[product.DeCate].Brands, product.DeEqui)
			} else {
				brands = *(*Categories)[product.DeCate].Brands
			}
		}
		(*Categories)[product.DeCate] = Category{Name: product.DeCate, Brands: &brands}
	}

}

// GetAllCategories extracts only categories
func GetAllCategories() []string {
	var categories []string
	var categoriesMap = make(map[string]bool)
	for _, product := range Products {
		if categoriesMap[product.DeCate] == false {
			categoriesMap[product.DeCate] = true
			if product.DeCate != "" {
				categories = append(categories, product.DeCate)
			}
		}
	}
	return categories
}

// GetAllBrands extracts only brands
func GetAllBrands() []string {
	var brands []string
	var brandsMap = make(map[string]bool)
	for _, product := range Products {
		if brandsMap[product.DeEqui] == false {
			brandsMap[product.DeEqui] = true
			brands = append(brands, product.DeEqui)
		}
	}
	return brands
}

// SearchByTerm search by contains substring in string
func SearchByTerm(term string, base *[]Product) []Product {

	var foundProducts []Product

	if base == nil {
		for _, product := range Products {
			if strings.Contains(product.DeItem, term) {
				foundProducts = append(foundProducts, product)
			}
		}
	} else {
		for _, product := range *base {
			if strings.Contains(product.DeItem, term) {
				foundProducts = append(foundProducts, product)
			}
		}
	}

	return foundProducts
}

// FilterProducts filter products by cat, bra, fam
func FilterProducts(categories *[]string, brands *[]string, families *[]string) *[]Product {

	var products = []Product{}

	if len(*families) == 0 {
		if len(*brands) == 0 {
			brands = getBrands(categories)
		}
		families = getFamilies(brands)
	}

	var productIds = make(map[int32]bool)

	for _, family := range *families {
		for _, productID := range *(Families[family].ProductIDs) {
			if _, ok := productIds[productID]; ok == false {
				productIds[productID] = true
			}
		}
	}

	for productID := range productIds {
		products = append(products, ProductsMap[productID])
	}

	return &products
}

func getFamilies(brands *[]string) *[]string {

	var preselectedRawFamilies = []string{}
	for _, brand := range *brands {
		preselectedRawFamilies = append(preselectedRawFamilies, *(Brands[brand].Families)...)
	}

	var selectedFamilies []string
	var m = make(map[string]bool)
	for _, family := range preselectedRawFamilies {
		if _, ok := m[family]; !ok == true {
			m[family] = true
			selectedFamilies = append(selectedFamilies, family)
		}
	}

	return &selectedFamilies
}

func getBrands(categories *[]string) *[]string {

	var preselectedRawBrands = []string{}
	for _, category := range *categories {
		preselectedRawBrands = append(preselectedRawBrands, *(Categories[category].Brands)...)
	}

	var selectedBrands []string
	var m = make(map[string]bool)
	for _, brand := range preselectedRawBrands {
		if _, ok := m[brand]; !ok == true {
			m[brand] = true
			selectedBrands = append(selectedBrands, brand)
		}
	}

	return &selectedBrands
}

func HasNextChunk(products *[]Product, next int, bulkSize int) bool {
	start := (next + 1)*bulkSize
	end := start + bulkSize

	if len(*products) >= end {
		return true
	}
	if len(*products) > start && len(*products) <= end {
		return true
	}	
	return false
}

func HasChunk(products *[]Product, next int, bulkSize int) (bool, int) {
	start := next*bulkSize
	end := start + bulkSize

	if len(*products) >= end {
		return true, -1
	}
	if len(*products) > start && len(*products) <= end {
		return true, len(*products)
	}	
	return false, -1
}

func GetChunk(products *[]Product, next int, bulkSize int)  *[]Product {
	
	start := next*bulkSize
	end := start + bulkSize

	if has, limit := HasChunk(products, next, bulkSize); has == true {
		var chunk []Product
		if limit != -1 {
			chunk =  (*products)[start:limit]
		} else {
			chunk =  (*products)[start:end]
		}
		return &chunk 
	} else {
		return &([]Product{})
	}
}
