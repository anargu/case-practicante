package provider

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAllCategories(t *testing.T) {

	Products = []Product{
		{IDItem: 1, DeItem: "producto 1", DeCate: "productocat 1", DeEqui: "productequi 1", DeFami: "productfami 1"},
		{IDItem: 2, DeItem: "producto 2", DeCate: "productocat 2", DeEqui: "productequi 2", DeFami: "productfami 2"},
		{IDItem: 3, DeItem: "producto 3", DeCate: "productocat 1", DeEqui: "productequi 3", DeFami: "productfami 3"},
		{IDItem: 4, DeItem: "producto 4", DeCate: "productocat 4", DeEqui: "productequi 4", DeFami: "productfami 4"},
		{IDItem: 5, DeItem: "producto 5", DeCate: "productocat 2", DeEqui: "productequi 5", DeFami: "productfami 5"},
	}

	expected := []string{"productocat 1", "productocat 2", "productocat 4"}
	actual := GetAllCategories()
	assert.Equal(t, expected, actual)
}

func TestGetAllBrands(t *testing.T) {

	Products = []Product{
		{IDItem: 1, DeItem: "producto 1", DeCate: "productocat 1", DeEqui: "productequi 1", DeFami: "productfami 1"},
		{IDItem: 2, DeItem: "producto 2", DeCate: "productocat 2", DeEqui: "productequi 2", DeFami: "productfami 2"},
		{IDItem: 3, DeItem: "producto 3", DeCate: "productocat 1", DeEqui: "productequi 1", DeFami: "productfami 3"},
		{IDItem: 4, DeItem: "producto 4", DeCate: "productocat 4", DeEqui: "productequi 1", DeFami: "productfami 4"},
		{IDItem: 5, DeItem: "producto 5", DeCate: "productocat 2", DeEqui: "productequi 4", DeFami: "productfami 5"},
	}

	expected := []string{"productequi 1", "productequi 2", "productequi 4"}
	actual := GetAllBrands()
	assert.Equal(t, expected, actual)
}

func TestSomthg(t *testing.T) {

	Products = []Product{
		{IDItem: 1, DeItem: "producto 1", DeCate: "productocat 1", DeEqui: "productequi 1", DeFami: "productfami 1"},
		{IDItem: 2, DeItem: "producto 2", DeCate: "productocat 2", DeEqui: "productequi 2", DeFami: "productfami 2"},
		{IDItem: 3, DeItem: "producto 3", DeCate: "productocat 1", DeEqui: "productequi 1", DeFami: "productfami 3"},
		{IDItem: 4, DeItem: "producto 4", DeCate: "productocat 4", DeEqui: "productequi 1", DeFami: "productfami 4"},
		{IDItem: 5, DeItem: "producto 5", DeCate: "productocat 2", DeEqui: "productequi 4", DeFami: "productfami 5"},
	}

	var Families FamilyMap = make(FamilyMap)

	InitAllFamilies(&Families)

	actual := Families
	expected := FamilyMap{
		"productfami 1": {Name: "productfami 1", Brand: "productequi 1", Category: "productocat 1", ProductIDs: &[]int32{1}},
		"productfami 2": {Name: "productfami 2", Brand: "productequi 2", Category: "productocat 2", ProductIDs: &[]int32{2}},
		"productfami 3": {Name: "productfami 3", Brand: "productequi 1", Category: "productocat 1", ProductIDs: &[]int32{3}},
		"productfami 4": {Name: "productfami 4", Brand: "productequi 1", Category: "productocat 4", ProductIDs: &[]int32{4}},
		"productfami 5": {Name: "productfami 5", Brand: "productequi 4", Category: "productocat 2", ProductIDs: &[]int32{5}},
	}
	assert.Equal(t, expected, actual)

}
