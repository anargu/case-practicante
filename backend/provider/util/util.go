package util

func IndexOfString(slice *[]string, element string) int {
	for i, item := range *slice {
		if item == element {
			return i
		}
	}
	return -1
}
