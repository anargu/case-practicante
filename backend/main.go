package main

import (
	"fmt"
	"strings"
	"strconv"
	. "./provider"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	// gin.SetMode(gin.DebugMode)

	InitData()

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:8080"}
	r.Use(cors.New(config))

	r.GET("/search", func(c *gin.Context) {

		term := c.Query("term")
		rawCategories := c.Query("categories")
		rawBrands := c.Query("brands")
		rawFamilies := c.Query("families")
		nextParam := c.DefaultQuery("next", "0")
		next, err := strconv.Atoi(nextParam)
		if err != nil{
			fmt.Println("ERROR***")
			fmt.Println(err)
		}

		var categoriesSelected []string
		var brandsSelected []string
		var familiesSelected []string

		if rawCategories != ""{
			categoriesSelected = strings.Split(rawCategories, ",")
		}
		if rawBrands != ""{
			brandsSelected = strings.Split(rawBrands, ",")
		}
		if rawFamilies != ""{
			familiesSelected = strings.Split(rawFamilies, ",")
		}

		var filteredProducts *[]Product

		if len(categoriesSelected) > 0 {
			filteredProducts = FilterProducts(&categoriesSelected, &brandsSelected, &familiesSelected)
		}

		products := SearchByTerm(term, filteredProducts)
		chunkSize := 25

		nextChunk := -1
		if HasNextChunk(&products, next, chunkSize) {
			nextChunk = next + 1
		}
		products = *(GetChunk(&products, next, chunkSize))
		
		var response = QueryResponse{Data: products, RelatedSearch: nil, Next: nextChunk}

		c.JSON(200, response)
	})

	r.GET("/products", func(c *gin.Context) {

		rawCategories := c.Query("categories")
		rawBrands := c.Query("brands")
		rawFamilies := c.Query("families")

		categoriesSelected := strings.Split(rawCategories, ",")
		brandsSelected := strings.Split(rawBrands, ",")
		familiesSelected := strings.Split(rawFamilies, ",")

		if len(categoriesSelected) > 0 {
			products := FilterProducts(&categoriesSelected, &brandsSelected, &familiesSelected)
			c.JSON(200, *products)
		} else {
			c.JSON(200, Products)
		}
	})

	r.GET("/families", func(c *gin.Context) {
		c.JSON(200, Families)
	})

	r.GET("/categories", func(c *gin.Context) {
		c.JSON(200, Categories)
	})

	r.GET("/brands", func(c *gin.Context) {
		c.JSON(200, Brands)
	})

	r.Run(":3000")
}
