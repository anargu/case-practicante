# Simple Product Search (Backend)

> search experience sample (backend)
## Features
written in Go
no database engine used

## Requirements
go = 1.10.2 (linux) [go to this link](https://golang.org/doc/install?download=go1.10.2.linux-amd64.tar.gz) 

[gin gonic (Web Framework)](https://gin-gonic.github.io/gin/)
``` bash
$ go get github.com/gin-gonic/gin
```
[gin gonic cors](https://github.com/gin-contrib/cors) (to avoid some origin conflicts)
``` bash
$ go get github.com/gin-contrib/cors
```
[testify](https://github.com/stretchr/testify) (testing)
``` bash
$ go get github.com/stretchr/testify
```


## Steps to run in development mode

``` bash
# in backend directory. backend will serve in port 3000
$ go run main.go

```

## Steps to run in production mode

``` bash
# in terminal
$ export GIN_MODE=release; go run main.go
# if you are on Windows try to execute ./debug file, if it doesn't start just rebuild the code 
```

For detailed explanation on how things work, contact to me at this email anthony.arostegui@gmail.com
