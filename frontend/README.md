# Simple Product Search 

> search experience sample
## Features
PWA (Progresive web app)
Responsive

## Requirements
node.js >= 8.x
npm >= 5.2
yarn 1.6.0 (last)


## Steps to run in development mode

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev dev

```

## Steps to run in production mode

``` bash
# install dependencies
yarn

# build in production files 
yarn build

# go to /dist directory
cd dist/
# serve index.html and static files with python 2.7, just make sure to run this command in /dist directory
python -m SimpleHTTPServer 8080
# if python version is 3.5, run this
python -m http.server 8080
#then access to...
http://localhost:8080
```
If you are using Windows, please refer to this [link](https://gist.github.com/jgravois/5e73b56fa7756fd00b89) to serve the Web App (the most simple way is the pythonic way.


For detailed explanation on how things work, contact to me at this email anthony.arostegui@gmail.com
