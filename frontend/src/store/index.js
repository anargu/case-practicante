import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = () => ({
  test: 'test',
  BASE_URL: 'http://127.0.0.1:3000',
  categories: {},
  baseBrands: {},
  brands: {},
  families: [],
  familyFilter: [],
  brandFilter: [],
  categoryFilter: [],
  isLoading: false,
  isSnackbar: false,
  sbText: '',
  mapKeyWords: {
    'families': 'familyFilter',
    'brands': 'brandFilter',
    'categories': 'categoryFilter'
  }
})

const getters = {
  categoriesSelected (state) {
    return state.categoryFilter
  },
  brandsSelected (state) {
    return state.brandFilter
  },
  familiesSelected (state) {
    return state.familyFilter
  },
  categories (state) {
    return state.categories
  },
  brands (state) {
    return state.brands
  },
  families (state) {
    return state.families
  }
}

const actions = {
  deleteItem ({state}, {item, listType}) {
    const key = state.mapKeyWords[listType]
    const indexFamily = state[key].indexOf(item)
    if (indexFamily !== -1) {
      state[key].splice(indexFamily, 1)
    }
  },
  onSelectAnItem ({state}, {item, listType}) {
    const key = state.mapKeyWords[listType]
    const indexFamily = state[key].indexOf(item)

    if (listType === 'categories') {
      if (indexFamily === -1) {
        state[key].push(item)
      } else {
        state[key].splice(indexFamily, 1)
      }
      // categories selected
      state.brands = {}
      state[key].map(category => {
        state.categories[category.name].brands.map(brand => {
          state.brands[brand] = state.baseBrands[brand]
        })
      })
    } else if (listType === 'brands') {
      if (indexFamily === -1) {
        state[key].push(item)
      } else {
        state[key].splice(indexFamily, 1)
      }

      // brands selected
      state.families = []

      state[key].map(brand => {
        brand.families.map(family => {
          if (state.families.indexOf(family) === -1) {
            state.families.push(family)
          }
        })
      })
      state.families.sort()
    } else if (listType === 'families') {
      if (indexFamily === -1) {
        state[key].push(item)
      } else {
        state[key].splice(indexFamily, 1)
      }
    }
  },
  showSnackbar ({state}, message) {
    state.sbText = message
    state.isSnackbar = true

    setTimeout(() => {
      state.isSnackbar = false
    }, 3000)
  },
  async fetchFilters ({state, dispatch}) {
    state.isLoading = true
    await axios.get(state.BASE_URL + '/categories')
      .then(res => {
        state.categories = res.data
      })
      .catch(err => {
        dispatch('showSnackbar', 'Error de conexion. Intente de nuevo en unos minutos')
        console.log('error***', err)
      })

    await axios.get(state.BASE_URL + '/brands')
      .then(res => {
        state.baseBrands = res.data
      })
      .catch(err => {
        dispatch('showSnackbar', 'Error de conexion. Intente de nuevo en unos minutos')
        console.log('error***', err)
      })

    state.isLoading = false
  }
}

const store = new Vuex.Store({
  state,
  actions,
  getters,
  modules: {
  }
})

export default store
