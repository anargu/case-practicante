console.time("query")
const products = require('./products.json')

//console.log(products)

let categories = []
let brands = []
let families = []
let namesWithNumbers = []
let nameLength3 = 0
let nameLength4 = 0

products.map(product => {

	let name = ''
	for (let index = 0; index < product.DE_ITEM.length; index++) {
		const char = product.DE_ITEM[index];
		if (!isNaN(char)) {
			name += char
		}
	}

	if (product.DE_ITEM.indexOf("     ") !== -1) {
		namesWithNumbers.push(product.DE_ITEM)
	}

	if ((product.DE_ITEM.length === 3) ) {
		if (!isNaN(product.DE_ITEM)) {
			console.log('*** product.DE_ITEM', product.DE_ITEM)
			nameLength3++
		}
	}
	if ((product.DE_ITEM.length === 4)) {
		if (!isNaN(product.DE_ITEM)) {
			console.log('**** product.DE_ITEM', product.DE_ITEM)
			nameLength4++
		}
	}

	if (families.indexOf(product.DE_FAMI) === -1) {
		families.push(product.DE_FAMI)
	}

	if (categories.indexOf(product.DE_CATE) === -1) {

		categories.push(product.DE_CATE)
	}

	if (brands.indexOf(product.DE_EQUI) === -1) {
		brands.push(product.DE_EQUI)
	}
})

console.log('category size: ', categories.length)
console.log('brand size: ', brands.length)
console.log('family size: ', families.length)


console.log('categories: ', categories.sort())
//  console.log('names with numbers', namesWithNumbers)
// console.log('nameLength3', nameLength3)
// console.log('nameLength4', nameLength4)

console.timeEnd("query")